



------------------------------------------------------------------------------------------------------

REQUIREMENTS:
QR CODE scanner apk - https://play.google.com/store/apps/details?id=com.gamma.scan&hl=en_US
QR GENERATOR	    - https://www.qr-code-generator.com/
XAMPP		    - https://www.apachefriends.org/index.html

-------------------------------------------------------------------------------------------------------
Instruction:
1. Put your project inside the htdocs folder and share the folder to the network.
2. Get your IP address using command prompt.
3. Generate a random QR code using http://YOURIPADDRESS/FOLDERNAME/attendance.php?id=RANDOMNUMBER
4. Scan the QR code. :)

Note: "YOURIPADDRESS" , "FOLDERNAME" , "RANDOMNUMBER" can be change depending on your set-up
